'use strict';

const gulp = require('gulp');
const babel = require('gulp-babel');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const cleancss = require('gulp-clean-css');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');
const browserSync = require('browser-sync').create(),
      reload = browserSync.reload;

const webpack = require('webpack-stream');
const { VueLoaderPlugin } = require('vue-loader');
const sourcemaps = require('gulp-sourcemaps');

/**
 * Here we set a prefix for our compiled and stylesheet and scripts.
 * Note that this should be the same as the `$themeHandlePrefix` in `func-script.php` and `func-style.php`.
 */
const themePrefix = 'fida';

/**
 * Paths and files
 */
const srcScss = 'scss/**/*.scss';
const srcJsDir = 'js';
const srcJsFiles = [
    //`./node_modules/babel-polyfill/dist/polyfill.js`,
    `${srcJsDir}/scripts/common.js`,
    `${srcJsDir}/scripts/map.js`,
    `${srcJsDir}/scripts/product.js`,
];
const srcJsDevFiles = [...srcJsFiles, `${srcJsDir}/scripts/dev.js`];
const destCss = 'css';
const destJs = 'js';

	// browser-sync task for starting the server.
gulp.task('browser-sync', function() {
    //watch files
    var files = [
        './**/*.php',
        './*.php'
    ];
    
    //initialize browsersync
    browserSync.init(files, {
        //browsersync with a php server
        proxy: "https://fida.local/",
        notify: true
    });
});

/**
 * Task for styles.
 *
 * Scss files are compiled and sent over to `assets/css/`.
 */
gulp.task('css',  () => {
    return gulp.src(srcScss)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({ cascade : false }))
        .pipe(rename(`${themePrefix}.min.css`))
        .pipe(cleancss())
        .pipe(gulp.dest(destCss))
		.pipe(browserSync.stream())

});

/**
 * Task for scripts.
 *
 * Js files are uglified and sent over to `assets/js/scripts/`.
 */
gulp.task('js', () => {
    return gulp.src(srcJsFiles)
        .pipe(babel({
            presets : ['es2015']
        }))
        .pipe(concat(`${themePrefix}.min.js`))
        .pipe(uglify())
        .pipe(gulp.dest(destJs))
		.pipe(browserSync.stream())

});


/**
 * Task for scripts.
 *
 * Js files are uglified and sent over to `assets/js/scripts/`.
 */
 gulp.task('js-dev', () => {
  return gulp.src(srcJsDevFiles)
      .pipe(sourcemaps.init())
      .pipe(babel({
          presets : ['es2015']
      }))
      .pipe(concat(`${themePrefix}.min.js`))
      .pipe(uglify())
      .pipe(sourcemaps.write())
      .pipe(gulp.dest(destJs))
  .pipe(browserSync.stream())

});

/**
 * Task for watching styles and scripts.
 */
gulp.task('watch', () => {
    gulp.watch(srcScss, gulp.series('css'));
    gulp.watch(srcJsFiles, gulp.series('js-dev'));
});


gulp.task("vue", function() {
  return gulp.src('vue/main.js',)
          .pipe(webpack({
            watch: true,
            //devtool: "",
            devtool: "eval-source-map",
            optimization: {
              splitChunks: false
            },
            //mode: "production",
            mode: "development",
            module: {
              rules: [
                {
                  test: /\.vue$/,
                  loader: 'vue-loader',
                },
                {
                  test: /\.pug$/,
                  loader: 'pug-plain-loader'
                }
              ]
            },
            plugins: [
              new VueLoaderPlugin(),

            ],
            resolve: {
              alias: {
                'vue$': 'vue/dist/vue.esm.js'
              }
            },
          }))
          .pipe(rename('app.js'))
          .pipe(gulp.dest('dist/js'))
          .pipe(browserSync.stream())
})


gulp.task("vue-build", function() {
  return gulp.src('vue/main.js',)
          .pipe(webpack({
            watch: true,
            devtool: "",
            //devtool: "eval-source-map",
            optimization: {
              splitChunks: false
            },
            mode: "production",
            //mode: "development",
            module: {
              rules: [
                {
                  test: /\.vue$/,
                  loader: 'vue-loader',
                },
                {
                  test: /\.pug$/,
                  loader: 'pug-plain-loader'
                }
              ]
            },
            plugins: [
              new VueLoaderPlugin(),

            ],
            resolve: {
              alias: {
                'vue$': 'vue/dist/vue.esm.js'
              }
            },
          }))
          .pipe(rename('app.js'))
          .pipe(gulp.dest('dist/js'))
})


/**
 * Default task
 */
gulp.task('default', gulp.parallel('browser-sync', 'vue', 'css', 'js-dev', 'watch'));

/**
 * Build for production
 */
gulp.task('build', gulp.parallel('css', 'js', 'vue-build') );
