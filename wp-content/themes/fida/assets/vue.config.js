﻿
module.exports = {
  outputDir: 'vue-dist',
  filenameHashing: false,
  configureWebpack: {
    devtool: "eval-source-map",
    //devtool: "",
    optimization: {
      splitChunks: false
    },
    resolve: {
      alias: {
        //'vue$': 'vue/dist/vue.min.js'
        'vue$': 'vue/dist/vue.esm.js'
      }
    },
  }, 
}