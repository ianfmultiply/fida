import Vue from 'vue'
import Vuex from 'vuex'
import 'core-js/stable';
import 'regenerator-runtime/runtime';

Vue.use(Vuex)

Vue.config.productionTip = false;

window.addEventListener("DOMContentLoaded", function() {
  if(document.querySelectorAll("#vue-app").length) {
    new Vue({
    }).$mount('#vue-app')
  };  
  if(document.querySelectorAll(".vue-app").length) {
    new Vue({
    }).$mount('.vue-app')
  };  
})