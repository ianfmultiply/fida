const common = (($) => {
  "use strict";

  const handleDevUtilities = () => {

    if(window.location.host.indexOf(":3000") > -1) {
      const newUrl = `https://fida.local${window.location.pathname}`;
      $("body").prepend(`<a class="utility-link" href="${newUrl}">Open as Fida.local</a>`);
    } else if (window.location.host.indexOf("fida.local") > -1) {
      const newUrl = `https://fida.local:3000${window.location.pathname}`;
      $("body").prepend(`<a class="utility-link" href="${newUrl}">Open as BrowserSync</a>`);
    }
  }

  /** 
   * Fire events on document ready and bind other events
   *
   * @since   1.0.0
   */
  const ready = () => {
    
    handleDevUtilities();
  };
 
  /**
   * Only expose the ready function to the world
   */
  return {
    ready: ready,
  };
})(jQuery);

jQuery(common.ready);
