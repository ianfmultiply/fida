class Retailers {

  constructor () {
    this.milesMultiplier = 3959
    this.centerLatitude = 54.390020
    this.centerLongitude = -4.189840
    this.mapZoom = 5
    this.retailerData = [];
    this.isInternetExplorer11 = navigator.userAgent.toLowerCase().indexOf('trident') > -1

    this.mapOptions = {
      center: new google.maps.LatLng(this.centerLatitude, this.centerLongitude),
      zoom: this.mapZoom,
      panControl: false,
      zoomControl: true,
      mapTypeControl: false,
      streetViewControl: false,
      scrollwheel: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP//,
      //styles: style
    }

    this.markers = []

    /*if( $( window ).width() < 769 ) {
      this.center_latitude = 51.390020
      this.center_longitude = -4.189840
    }*/
  }

  init () {

    const self = this

    /* contact map*/
    this.map = new google.maps.Map(document.getElementById('map-canvas'), this.mapOptions)
    this.geocoder = new google.maps.Geocoder()


    jQuery.get('/wp-json/sleepeezee/v1/map', function(data) {
      self.retailerData = data;
      
      if( jQuery('#map-canvas').length) {

        jQuery('form.retailer-search').on('submit', (e) => {
          e.preventDefault()
          e.stopPropagation()

          jQuery('.find-bt').addClass('is-loading')

          if( jQuery('input[name="location"]').val() != "" ){

            self.resetMap()

            const address = jQuery('input[name="location"]').val() + ", United Kingdom"

            self 
              .geocoder
              .geocode(
                {
                  address
                },
                (results, status) => {
                  if (status === 'OK') {
                    self.map.setCenter(results[0].geometry.location);
                    //this.zoomMap()

                    let lat = results[0].geometry.location.lat();
                    let lng = results[0].geometry.location.lng();

                    const coords = new google.maps.LatLng(lat, lng);

                    const marker = new google.maps.Marker({
                      map: this.map,
                      position: results[0].geometry.location,
                      label: 'Test',
                      visible: true,
                      zIndex: 3
                    }) 

                    console.log({pouet: results[0].geometry.location, lat, lng, coords});
                    self.setMarkerColour(marker, 'your-location')

                    self.markers.push(marker)
                    self.bounds.extend(marker.position)

                    self.filterLocations(results[0].geometry.location)

                    if (self.markers.length == 1) {
                      jQuery('#error-msg').show()
                    }

                    jQuery('.find-bt').removeClass('is-loading')

                  } else {
                  // alert('Geocode was not successful for the following reason: ' + status);
                  }
                });
            }
        })


        if ("geolocation" in navigator) {
          jQuery('#get-current-location').on('click',(e) => {
            e.preventDefault()
            e.stopPropagation()

            self.resetMap()

            jQuery('.find-bt').addClass('is-loading')

            jQuery('input[name="location"]').val('Current Location')

            navigator.geolocation.getCurrentPosition((position) => {

              const coords = new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
                    marker = new google.maps.Marker({
                      map: self.map,
                      position: coords,
                      label: '',
                      visible: true,
                      zIndex: 1
                    })

              self.setMarkerColour(marker, 'your-location')
              self.markers.push(marker)
              self.map.setCenter(coords)

              self.bounds.extend(marker.position)

              self.filterLocations(coords)

              if (self.markers.length == 1) {
                jQuery('#error-msg').show()
              }

              jQuery('.find-bt').removeClass('is-loading')


            })
          })
        } else {
          jQuery('#get-current-location').parents('.control').hide()
        }

      }
    });

  }

  zoomMap() {
    if( jQuery( window ).width() < 1088){
      this.map.setZoom(8);
    } else {
      this.map.setZoom(10);
    }
  }

  calculateDistanceFromCenter(center, coords) {
    return (
      this.milesMultiplier *
      Math.acos(
        Math.cos(
          this.degreesToRadians(center.lat())
        ) *
        Math.cos(
          this.degreesToRadians( coords.lat() )
        ) *
        Math.cos(
          this.degreesToRadians( coords.lng() ) -
          this.degreesToRadians( center.lng() )
        ) +
        Math.sin(
          this.degreesToRadians( center.lat() )
        ) *
        Math.sin(
          this.degreesToRadians( coords.lat() )
        )
      )
    )
  }

  degreesToRadians(degrees) {
    return degrees * (Math.PI/180)
  }

  resetMap() {
    this.markers.forEach((marker) => {
      marker.setMap(null)
    })

    this.markers = []
    this.bounds = new google.maps.LatLngBounds()
    jQuery('#error-msg').hide()
  }

  

  filterLocations(center) {

    const self = this

    const infowindow = new google.maps.InfoWindow({content: ""})

    window.selectedRetailers = [];

    for (const retailer of this.retailerData) {
      this.handleRetailer(retailer, infowindow, self, center);        
    }

    jQuery('#find-store-results').empty();

    jQuery('.results__title').html(`${window.selectedRetailers.length} Stores Near You`);

    window.selectedRetailers = window.selectedRetailers.sort(function(a, b) { return a.distance - b.distance; });

    for(let i = 0; i < window.selectedRetailers.length; i++) {
      let retailer = window.selectedRetailers[i];
      const html = self.getContentForRetailer(retailer);
      jQuery('#find-store-results').append(html);
    }

  }

  getContentForRetailer(retailer) {
    let icon = "";
    let category = "National Retailer";


    if (retailer.platinum_retailer == "1") {
      icon = `/wp-content/themes/sleepeezee/assets/images/platinum.svg`;
      category = "Platinum Retailer";
    }
    else if (retailer.national_retailer !== "N/A") {
      icon = `/wp-content/themes/sleepeezee/assets/images/national.svg`;
      category = "National Retailer";
    }
    else{
      icon = `/wp-content/themes/sleepeezee/assets/images/retailer.svg`;
      category = "Retailer";
    }

    let queryString = encodeURI(`${retailer.address1} ${retailer.address2}, ${retailer.city}, ${retailer.postcode}`);
    let mapslink = `https://www.google.com/maps/search/?api=1&query=${queryString}`;

    return `
    <div class="result__container">
      <div class="result__category">
        <img src="${icon}" alt=""> ${category}
      </div>
      <div class="result__name">${retailer.name} - ${retailer.distance.toFixed(1)}m away</div>
      <div class="result__address">${retailer.address1} ${retailer.address2} ${retailer.city} ${retailer.postcode}</div>
      <div class="result__ctas">
        <a href="${mapslink}" target='_blank'>Get Directions</a>
        <a href="${retailer.website}"  target='_blank'>Go to site</a>
      </div>
    </div>
    `;
  }

  handleRetailer(retailer, infowindow, self, center) {
    const latitude = retailer.geolocation.lat,
    longitude = retailer.geolocation.lng,
    distance = self.calculateDistanceFromCenter(
      center,
      new google.maps.LatLng(latitude, longitude)
    )

//    console.log('distance', distance)

    retailer.distance = distance;

    if (distance < 25) {
    window.selectedRetailers.push(retailer);
    const label = retailer.index,
          locationContent = this.getContentForRetailer(retailer),
          marker = new google.maps.Marker({
            position: new google.maps.LatLng(latitude, longitude),
            map: self.map,
            visible: true,
            content: locationContent,
            label: '', //$label,
            zIndex: 2
          })

    //console.log('has-product', retailer.products !== null)

    if (retailer.platinum_retailer == "1") {
      marker.setIcon(`/wp-content/themes/sleepeezee/assets/images/platinum.svg`);
    }
    else if (retailer.national_retailer !== "N/A") {
      self.setMarkerColour(marker, 'national')
    }
    else{
      if (jQuery(this).attr('data-has-product')) {
        self.setMarkerColour(marker, 'retailer')
      } else {
        self.setMarkerColour(marker, 'retailer')
      }
    }


    google.maps.event.addListener(marker, 'click', () => {
      infowindow.setContent(`<div class="scrollFix">${marker.content}</div>`)
      infowindow.open(self.map, marker)
    })

    marker.setMap(self.map)

    //extend the bounds to include each marker's position
    self.bounds.extend(marker.position)

    self.markers.push(marker)

    }

    self.map.fitBounds(self.bounds)
    self.zoomMap()
  } 

  setMarkerColour(marker, colour) {
    marker.setIcon(`/wp-content/themes/sleepeezee/assets/images/${colour}.svg`)
  }

}


const common = (($) => {
  "use strict";


  const ready = () => {
   let retailers = new Retailers();

   retailers.init();
  };


  return {
    ready: ready,
  };
})(jQuery);

jQuery(common.ready);
