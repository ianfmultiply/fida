const common = (($) => {
  "use strict";

  const updateImage = (url) => {
    $(".lightbox__image--img").attr("src", url);
  };

  const stripHtml = (html) => {
    let tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || "";
  };

  const getDisplayedPrice = () => {
    return +stripHtml($(".woocommerce-variation-price .woocommerce-Price-amount.amount bdi").html()).replace(',', '').substr(1);
  };

  const getRecyclingPrice = () => {
    return +$("#recyclingprice").val();
  };

  const getPriceWithRecyclingIfNeeded = (originalPrice, needsRecycling) => {

    let recyclingPrice = getRecyclingPrice();
    let totalPrice = +originalPrice + recyclingPrice * needsRecycling;

    let formattedPrice = totalPrice.toFixed(2);
    return `£${formattedPrice}`;

  };

  const handleProductImages = (allUrls) => {

    let currentIndex = 0;
    const maxIndex = allUrls.length;

    $(".product-gallery__image").click(function() {
      const fullImageUrl = $(this).data("full");

      currentIndex = allUrls.indexOf(fullImageUrl);

      updateImage(fullImageUrl);
 
      $(".lightbox__container").fadeIn();

    });

    $(".lightbox__close, .lightbox__overlay").click(function(e) {
      $(".lightbox__container").fadeOut(500);
    });

    $(".lightbox__container i.next").click(function() {
      currentIndex = (currentIndex + 1) % maxIndex;
      updateImage(allUrls[currentIndex]);
    });

    $(".lightbox__container i.prev").click(function() {
      currentIndex = (currentIndex + 1) % maxIndex;
      updateImage(allUrls[currentIndex]);
    });

    let originalPrice = 0;
    let needsRecycling = false;

    $("#pa_bed-size").change(function() {
      setTimeout(function() {
        originalPrice = getDisplayedPrice();
        let totalPrice = getPriceWithRecyclingIfNeeded(originalPrice, needsRecycling);
        $(".woocommerce-Price-amount bdi").html(totalPrice.replace(/\B(?=(\d{3})+(?!\d))/g, ","));
      }, 10);
    });

    $('input:radio[name="attribute_pa_recycling"]').change(function() {
      if(originalPrice === 0) {
        originalPrice = getDisplayedPrice();
      }
      needsRecycling = $(this).val() == 'yes';
      setTimeout(function() {
        let totalPrice = getPriceWithRecyclingIfNeeded(originalPrice, needsRecycling);
        $(".woocommerce-Price-amount bdi").html(totalPrice.replace(/\B(?=(\d{3})+(?!\d))/g, ","));
      }, 10);
    });

  }

  /**
   * Fire events on document ready and bind other events
   *
   * @since   1.0.0
   */
  const ready = () => {
    let allData = [];
    $(".product-gallery__image")
    .each(function() {
      let data = $(this).data("full");
      allData = [...allData, data];
    });
    
   handleProductImages(allData);
  };

  /**
   * Only expose the ready function to the world
   */
  return {
    ready: ready,
  };
})(jQuery);

jQuery(common.ready);
