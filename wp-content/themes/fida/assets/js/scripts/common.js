const common = (($) => {
  "use strict";


  const manageChallengePopup = () => {
  
    $(".challenge-open").on("click tap", function() {
      let what = $(this).find(".chp_what").val();
      let when = $(this).find(".chp_when").val();
      let end = $(this).find(".chp_end").val();
      let image = $(this).find(".chp_image").val();
      let length = $(this).find(".chp_length").val();
      let url = $(this).find(".chp_url").val();

      //console.log({what, when, end, image, length, url});

      setTimeout(function() {
        $('.chp_field_what p').html(what);
        $('.chp_field_when p').html(when);
        $('.chp_field_end p').html(end);
        $('.chp_field_image p').html(`<img src="${image}" class='chp_round' />`);
        $('.chp_field_length p').html(length);
        $('.chp_link a').attr("href", url);
         
      }, 10);
      
      

    });
    
  }

  const toggleMobileMenu = () => {
    $(".burger-menu").on("click", function(e) {
      e.preventDefault();
      $(this).toggleClass("active");
      if($(this).hasClass("active")) {
        $("#menu-mobile-menu").addClass("active");
      } else {
        $("#menu-mobile-menu").removeClass("active");
      }
    })
    $('main').on("click", function() {
      $("#menu-mobile-menu").removeClass("active");
      $(".burger-menu").removeClass("active");
    })
  };

  const resizeSelector = (selector) => {
    let resize = () => {
      let heights = [];
      $(selector).height("auto");
      $(selector).each(function() {
        heights.push($(this).height());
      });
      let maxHeight = Math.max(...heights);
      $(selector).height(maxHeight);
    };

    resize();
    $(window).resize(function() { resize(); });

    
  };
  const equalCols = () => {

    resizeSelector(".fida_cta__title");
    resizeSelector(".fida_cta__content");
    resizeSelector(".card-title");
    resizeSelector(".card-content");
    
  };



  /**
   * Fire events on document ready and bind other events
   *
   * @since   1.0.0
   */
  const ready = () => {
    manageChallengePopup();
    toggleMobileMenu();
    equalCols();
  };

  /**
   * Only expose the ready function to the world
   */
  return {
    ready: ready,
  };
})(jQuery);

jQuery(common.ready);
