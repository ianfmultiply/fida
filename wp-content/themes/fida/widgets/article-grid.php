<?php 
   $the_query = new WP_Query( array(
     'posts_per_page' => 4,
  )); 
  $index = 1;
?>



  <?php if ( $the_query->have_posts() ) : ?>
    <div class="article-grid">
      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
      <?php 
        $backgroundImage = get_the_post_thumbnail_url();
      ?>
      <a href="<?php echo get_the_permalink(); ?>" 
           class="ag__article article-<?php echo $index; ?>" 
           style="background: linear-gradient(8deg, #203c66, hsla(0, 0%, 100%, 0)), 
                              url(<?php echo $backgroundImage ?>) no-repeat center center / cover;">
        <div class="ag__text">
          <div class="ag__title">
            <?php the_title(); ?> 
          </div>
          <?php if($index == 1): ?>
          <div class="ag__content">
            <?php the_excerpt(); ?> 
          </div>
          <?php endif; ?>
        </div>
        <div class="ag__link">
          See more >
        </div>
      </a>

      <?php $index++; ?>
      <?php endwhile; ?>
      <?php wp_reset_postdata();?>

      <?php else : ?>
        <p><?php __('No News'); ?></p>
    </div>
  <?php endif; ?>

  <?php foreach($postslist as $index => $post): setup_postdata($post); ?>
    
  <?php endforeach;?>