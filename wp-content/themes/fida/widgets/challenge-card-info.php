<?php 
$challenge_explanation = get_field('challenge_explanation');
$open_date = get_field('open_date');
$what_happens_at_the_end = get_field('what_happens_at_the_end');
$challenge_picture = get_field('challenge_picture');
$completion_time = get_field('completion_time');
$url = get_the_permalink();

echo "<input class='chp_what' type='hidden' value='$challenge_explanation' />";
echo "<input class='chp_when' type='hidden' value='$open_date' />";
echo "<input class='chp_end' type='hidden' value='$what_happens_at_the_end' />";
echo "<input class='chp_image' type='hidden' value='$challenge_picture' />";
echo "<input class='chp_length' type='hidden' value='$completion_time' />";
echo "<input class='chp_url' type='hidden' value='$url' />";

?>
