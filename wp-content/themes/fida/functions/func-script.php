<?php
/**
 * Script functions
 *
 * @author   <Author>
 * @version  1.0.0
 * @package  <Package>
 */

/**
 * Enqueue theme scripts
 */
function gulp_wp_theme_scripts() {

	/**
	 * Set a script handle prefix based on theme name.
	 * Note that this should be the same as the `themePrefix` var set in your Gulpfile.js.
	 */
	$theme_handle_prefix = 'fida';

	/**
	 * Enqueue common scripts.
	 */
	wp_enqueue_script( $theme_handle_prefix . '-scripts', "/wp-content/themes/$theme_handle_prefix/assets/js/$theme_handle_prefix.min.js", array( 'jquery' ), '1.0.0', true );
	wp_enqueue_script( $theme_handle_prefix . '-vue', "/wp-content/themes/$theme_handle_prefix/assets/dist/js/app.js", array( 'jquery' ), '1.0.0', true );
	// $maps_key = get_field('maps_api_key', 'option');
	// wp_enqueue_script('google-maps', "https://maps.googleapis.com/maps/api/js?key=$maps_key", false, null, true);
}
add_action( 'wp_enqueue_scripts', 'gulp_wp_theme_scripts' );
