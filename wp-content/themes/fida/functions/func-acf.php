<?php

function my_acf_init() {
	$maps_api_key = get_field('maps_api_key', 'option');
	acf_update_setting('google_api_key', $maps_api_key);
}

add_action('acf/init', 'my_acf_init');