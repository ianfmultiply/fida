<?php
/**
 * Theme functions
 *
 * @author   <Author>
 * @version  1.0.0
 * @package  <Package>
 */
remove_theme_support( 'widgets-block-editor' );
require_once 'functions/func-acf.php';
require_once 'functions/func-admin.php';
require_once 'functions/func-customstyles.php';
require_once 'functions/func-debug.php';
require_once 'functions/func-images.php';
require_once 'functions/func-menu.php';
require_once 'functions/func-login.php';
require_once 'functions/func-script.php';
require_once 'functions/func-style.php'; 
require_once 'functions/func-woocommerce.php';
require_once 'functions/func-options.php';
require_once 'functions/func-user-registration.php';

